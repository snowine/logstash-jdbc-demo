package org.fmake.populator;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@RequiredArgsConstructor
public class PopulatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(PopulatorApplication.class, args);
    }

    @Bean
    CommandLineRunner populator(TestRepository repository) {
        return args -> {
            Thread.sleep(10000);
            for (int i = 1; i <= 300; i++) {
                repository.save(
                        Test.builder()
                                .id((long) i)
                                .name(RandomStringUtils.randomAlphabetic(10))
                                .build()
                );
                Thread.sleep(1000);
            }
        };
    }
}
