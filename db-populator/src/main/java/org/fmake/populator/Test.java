package org.fmake.populator;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@Builder
public class Test {
    @Id
    private Long id;
    private String name;
    @Version
    private Timestamp modStamp;
}
